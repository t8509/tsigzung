TsigZung: Is a mobile application for both android and ios that focuses mainly on the Dzongkha
words. Bhutan's sole official and national language is Dzongkha, a Sino-Tibetan language
spoken by more than half a million people. With a tiny population, studying dzongkha is a
time-consuming activity for which there are insufficient resources. For eager language learners,
learning dzongkha is a difficult task. This app was created with the help of the module tutor and
DDC in order to improve language learning progress and to encourage adolescents to stay up
with the latest dzongkha word trends.
Admin: 
Link:adminpanel-tsigzung.netlify.app
Admin Credentials
Username: 12200046.gcit@rub.edu.bt
Password: nagato123

App promo url: https://www.youtube.com/watch?v=rn9pUJVVlVc&feature=youtu.be&ab_channel=JAMESSUBBA
Screens for the mobile app:
 ![Screenshot](screenshots/1.jpg)   ![Screenshot](screenshots/2.jpg)
 ![Screenshot](screenshots/3.jpg)   ![Screenshot](screenshots/4.jpg)
 ![Screenshot](screenshots/5.jpg)   ![Screenshot](screenshots/6.jpg)
 ![Screenshot](screenshots/7.jpg)   ![Screenshot](screenshots/8.jpg)
 ![Screenshot](screenshots/9.jpg)

Screens for the admin website:
 ![Screenshot](screenshots/Admin1.png)  ![Screenshot](screenshots/Admin2.png) 
 ![Screenshot](screenshots/Admin3.png)  ![Screenshot](screenshots/Admin4.png)
 ![Screenshot](screenshots/Admin5.png)  ![Screenshot](screenshots/Admin6.png) 
 ![Screenshot](screenshots/Admin7.png)     
poster:
 ![Screenshot](TsigZung.png)
